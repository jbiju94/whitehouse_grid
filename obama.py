from modules import power_stat as power

MASTER_NAME = "WhiteHouse_Grid"


print ("###### POWERING ON THE SYSTEM ######")
print ("------------------------------------")
print ("-------------------------------------")
print ("[" + MASTER_NAME + "]Power Module -> Started")
power.connect_to_db()
power.hello_server()
