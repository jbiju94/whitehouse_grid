from scrapy import Spider


class SpidySpider(Spider):
    name = "stack"
    allowed_domain = ["stackoverflow.com"]
    start_urls = [
        "http://stackoverflow.com/questions?pagesize=50&sort=newest",
    ]