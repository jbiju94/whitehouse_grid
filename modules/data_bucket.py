import pyrebase


class DataConnector:
    d_connect = None
    config = {
        "apiKey": "AIzaSyDUG1jmnk2Kspa9FI4k_aRhkxFK8BW1FvE",
        "authDomain": "whitehouse-grid-power.firebaseapp.com",
        "databaseURL": "https://whitehouse-grid-power.firebaseio.com/",
        "storageBucket": "gs://whitehouse-grid-power.appspot.com"
    }

    def __init__(self):
        pass

    @staticmethod
    def connect_to_bucket():
        DataConnector.d_connect = pyrebase.initialize_app(DataConnector.config)
        if DataConnector.d_connect:
            print "Pouring data into DATA BUCKET"

    @staticmethod
    def get_db_connector():
        return DataConnector.d_connect

    @staticmethod
    def push_to_db(data, ref_path):
        db = DataConnector.d_connect.database()
        db.child(ref_path).push(data)

    @staticmethod
    def set_to_db(data, ref_path):
        db = DataConnector.d_connect.database()
        db.child(ref_path).set(data)

    @staticmethod
    def update_to_db(data, ref_path):
        db = DataConnector.d_connect.database()
        db.child(ref_path).update(data)
